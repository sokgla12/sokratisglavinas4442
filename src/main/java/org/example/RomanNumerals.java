package org.example;

public class RomanNumerals {



    public int parse(String x) throws Exception {
        if (x == "I") {
            return 1;
        }
        if (x == "II") {
            return 2;
        }
        if (x == "III") {
            return 3;
        }
        if (x == "IV") {
            return 4;
        }
        if (x == "V") {
            return 5;
        }
        if (x == "VI") {
            return 6;
        }
        if (x == "VII") {
            return 7;
        }
        if (x == "VIII") {
            return 8;
        }
        if (x == "IX") {
            return 9;
        }
        if (x == "X") {
            return 10;
        }
        else throw new Exception("Not correct");
    }



}

