package org.example;

import org.junit.Test;

import static org.junit.Assert.*;

public class RomanNumeralsTest {

    RomanNumerals romanNumerals = new RomanNumerals();
    @Test
    public void IfNumberIsI () throws Exception {
        assertEquals(romanNumerals.parse("I"),1);
    }

    @Test
    public void IfNumberIsII () throws Exception {
        assertEquals(romanNumerals.parse("II"),2);
    }

    @Test
    public void IfNumberIsIII () throws Exception {
        assertEquals(romanNumerals.parse("III"),3);
    }

    @Test
    public void IfNumberIsIV () throws Exception {
        assertEquals(romanNumerals.parse("IV"),4);
    }
    @Test
    public void IfNumberIsV () throws Exception {
        assertEquals(romanNumerals.parse("V"),5);
    }
    @Test
    public void IfNumberIsVI () throws Exception {
        assertEquals(romanNumerals.parse("VI"),6);
    }
    @Test
    public void IfNumberIsVII () throws Exception {
        assertEquals(romanNumerals.parse("VII"),7);
    }
    @Test
    public void IfNumberIsVIII () throws Exception {
        assertEquals(romanNumerals.parse("VIII"),8);
    }
    @Test
    public void IfNumberIsIX () throws Exception {
        assertEquals(romanNumerals.parse("IX"),9);
    }
    @Test
    public void IfNumberIsX () throws Exception {
        assertEquals(romanNumerals.parse("X"),10);
    }

    @Test
    public void IfNumberIsNotX () throws Exception {
        assertEquals(romanNumerals.parse("X"),10);
    }

    @Test(expected = Exception.class)
    public void WrongTypeThrowException() throws Exception{
        assertEquals(romanNumerals.parse("12345"),10);
    }
}